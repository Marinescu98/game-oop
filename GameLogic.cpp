#include "GameLogic.h"

int GameLogic::chooseForPlayer()
{
	cout << "Attack: <1> sword ";
	if (player.CanAttack())
		cout << " | <3> magic " ;
	if (player.CanRun() && player.getHp()<40)
		cout << " | <2> run ";
	cout << endl;
	int ch;
	cin >> ch;
	return ch;
}

bool GameLogic::Round4()
{

	cout << endl << "______ROUND 4_______" << endl;

	player.addMana(30);
	player.addHp(50);
	player.addArmor(50);

	Dragon dragon(60, 15, 1000, 20);

	player.PrintStats();
	cout << endl;
	dragon.PrintStats();
	cout << endl;

	int ch;

	int nr = 0, nrRun = 0;

	while (player.IsAlive() && dragon.IsAlive())
	{
		ch = chooseForPlayer();

		if (ch == 1)
		{
			player.Attack(dragon);
		}
		else {
			if (ch == 2 && nrRun <= 8)
			{
				player.Run();
				player.addMana(50);
				dragon.dragonFly();
				nrRun++;
			}
			else {
				if (ch == 3)
				{
					player.Magic(dragon);
				}
			}
		}

		if (ch != 2)
			player.TakeDamage(dragon.getDamage());

		if (nr % 2 == 0 && ch != 2)
		{
			player.TakeDamage(dragon.GetDamageAttackSpecial());
		}

		nr++;

		cout << endl;
		player.PrintStats();
		cout << endl;

		if (dragon.IsAlive())
		{
			dragon.PrintStats();
			cout << endl << endl;
		}
	}

	if (player.IsAlive())
	{
		cout << "\n\n+++++  Player won!!!  +++++\n\n";
		player.PrintStats();
		return true;
	}
	else
	{
		cout << "\n\n______ You're dead! The dragon won! :( _____\n\n ";
		player.PrintStats();
		return false;
	}

}

bool GameLogic::Round3()
{

	cout << endl << "______ROUND 3_______" << endl;

	player.addHp(50);
	player.setAttackSpecial();

	Shaman shaman(30, 10,80, 20);

	player.PrintStats();
	cout << endl;
	shaman.PrintStats();
	cout << endl;

	int ch;

	int nr = 0, nrRun = 0;

	while (player.IsAlive() && shaman.IsAlive())
	{
		ch = chooseForPlayer();

		if (ch == 1)
		{
			player.Attack(shaman);
		}
		else {
			if (ch == 2 && nrRun <= 2)
			{
				player.Run();
				nrRun++;
			}
			else {
				if (ch == 3)
				{
					player.Magic(shaman);
				}
			}
		}

		if (ch != 2)
			player.TakeDamage(shaman.getDamage());

		if (nr%2==0 && ch != 2)
		{
			player.TakeDamage(shaman.GetDamageAttackSpecial());
		}

		nr++;

		cout << endl;
		player.PrintStats();
		cout << endl;

		if (shaman.IsAlive())
		{
			shaman.PrintStats();
			cout << endl << endl;
		}
	}

	if (player.IsAlive())
	{
		cout << "\n\n+++++  Player won!!!  +++++\n\n";
		player.PrintStats();
		return true;
	}
	else
	{
		cout << "\n\n______ You're dead!! _____\n\n :(";
		player.PrintStats();
		return false;
	}

}

bool GameLogic::Round2()
{

	cout << endl << "______ROUND 2_______" << endl;

	player.addHp(20);
	player.setDamage(26);
	player.setRun();

	Orc orc(60, 10, 70, 50);

	player.PrintStats();
	cout << endl;
	orc.PrintStats();
	cout << endl;

	int ch;

	int nr = 0, nrRun=0;

	while (player.IsAlive() && orc.IsAlive())
	{
		ch = chooseForPlayer();

		if (ch == 1)
		{
			player.Attack(orc);
		}
		else {
			if (ch == 2 && nrRun<=2)
			{
				player.Run();
				nrRun++;
			}
		}

		if (ch != 2)
			player.TakeDamage(orc.getDamage());

		if (nr == 3)
		{
			player.TakeDamage(orc.GetdamageAttckSpecial());
		}

		nr++;

		cout << endl;
		player.PrintStats();
		cout << endl;
		if (orc.IsAlive())
		{
			orc.PrintStats();
			cout << endl << endl;
		}
	}

	if (player.IsAlive())
	{
		cout << "\n\n+++++  Player won!!!  +++++\n\n";
		player.PrintStats();
		return true;
	}
	else
	{
		cout << "\n\n______ You're dead!! _____\n\n :(";
		player.PrintStats();
		return false;
	}
	
}

bool GameLogic::Round1()
{
	cout << endl << "______ROUND 1_______" << endl;

	Goblin goblin1(20, 8, 50);
	Goblin goblin2(30, 5, 60);

	player.PrintStats();
	cout << endl;
	goblin1.PrintStats();
	cout << endl;
	goblin2.PrintStats();
	cout << endl;

	int ch, chm;

	int nr = 0;

	int GoblinDead1 = 0;
	int GoblinDead2 = 0;

	while (player.IsAlive() && (goblin1.IsAlive() || goblin2.IsAlive()))
	{
		ch = 1;

		cout << "Which monster want to attack? <1>/<2>  ";

		cin >> chm;

		if (chm == 1)
		{
			player.Attack(goblin1);
		}
		else {
			if (chm == 2)
			{
				player.Attack(goblin2);
			}
		}

		player.TakeDamage(goblin1.getDamage());
		player.TakeDamage(goblin2.getDamage());

		if (nr == 8)
		{
			goblin2.setXpForPlayer(40);
		}

		nr++;

		if (!goblin1.IsAlive() && !GoblinDead1)
		{
			player.SetEnergyXp(65, goblin1.GetXpForPlayer());
			player.addHp(40);
			GoblinDead1 = 1;
		}
		if (!goblin2.IsAlive() && !GoblinDead2)
		{
			player.SetEnergyXp(65, goblin2.GetXpForPlayer());
			player.addHp(40);
			GoblinDead2 = 1;
		}

		cout << endl;
		player.PrintStats();
		cout << endl;

		if (goblin1.IsAlive())
		{
			cout << "1. ";
			goblin1.PrintStats();
			cout << endl;
		}
		if (goblin2.IsAlive())
		{
			cout << "2. ";
			goblin2.PrintStats();
		}
		cout << endl << endl;
	}

	if (player.IsAlive())
	{
		cout << "\n\n+++++  Player won!!!  +++++\n\n";
		player.PrintStats();
		return true;
	}
	else
	{
		cout << "\n\n______ You're dead!! _____\n\n :(";
		player.PrintStats();
		return false;
	}
	
}