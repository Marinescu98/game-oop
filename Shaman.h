#pragma once
#include "Monster.h"
class Shaman :
	public Monster
{
	int damageAttackSpecial;

public:

	Shaman(int=0,int=0,int=0,int=0);

	void PrintStats()override;

	int GetDamageAttackSpecial()
	{
		return damageAttackSpecial;
	}
	
	~Shaman();
};

