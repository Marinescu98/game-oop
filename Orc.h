#pragma once
#include "Monster.h"

class Orc : public Monster
{
	int damageAttackSpecial;
public:
	Orc(int armour, int damage, int xp, int atk) : 
		Monster(armour,damage,xp)
		,damageAttackSpecial(atk)
	{}
	int GetdamageAttckSpecial()
	{
		return damageAttackSpecial;
	}
	void PrintStats() override;
};