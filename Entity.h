#pragma once

#include<iostream>

using namespace std;

class Entity
{
protected:
	int hp;
	int armour;
	int damage;
	bool alive;

public:

	Entity(int , int );

	inline bool IsAlive() const
	{
		return alive;
	}


	inline int getDamage()
	{
		return damage;
	}

	bool TakeDamage(const int&);
};