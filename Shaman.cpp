#include "Shaman.h"



Shaman::Shaman(int armour, int damage, int xp, int atk):
	Monster(armour,damage,xp)
	,damageAttackSpecial(atk)
{
}

void Shaman::PrintStats()
{
	cout << " Shaman " << '\n';
	Monster::PrintStats();
}

Shaman::~Shaman()
{
}
