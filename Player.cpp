#include "player.h"

Player::Player() :
	Entity(50, 25)
	, mana(100)
	, energy(100)
	, level(1)
	, energyForAttack(10)
	, manaForAttckSpecial(60)
	, xp(0)
	, attckSpecial(false)
	,run(false)
	,damageAttackSpecial(30)
{

}

void Player::Magic(Monster& enemy)
{
	if (mana >= manaForAttckSpecial)
	{
		enemy.TakeDamage(damageAttackSpecial);
		mana -= manaForAttckSpecial;
	}
	else
	{
		cout << "Low mana! Can not attack!\n";
	}
}

void Player::Attack(Monster& enemy)
{
	if (energy < energyForAttack)
	{
		std::cout << "Low energy! Can not attack!\n";
		return;
	}

	energy -= energyForAttack;
	enemy.TakeDamage(damage);

}


void Player::PrintStats()
{
	cout << "  *"<<name <<"*"<< endl;
	cout << "  -Hp = " << hp << endl;
	cout << "  -Armour = " << armour << endl;
	cout << "  -Mana = " << mana << endl;
	cout << "  -Energy = " << energy << endl;
	cout << "  -XP = " << xp << endl;
	cout << "  -Level = " << level << endl;

}

void Player::Run()
{
	hp += 30;
	energy += 20;
}