#pragma once
#include "Orc.h"
#include "Dragon.h"
#include "player.h"
#include"Shaman.h"
#include"Goblin.h"

class GameLogic
{
private:
	Player player;
public:
	GameLogic()
	{
		string name;
		cout << "Player's name is : ";
		getline(cin, name);
		player.setName(name);
	}

	int chooseForPlayer();
	bool Round1();
	bool Round2();
	bool Round3();
	bool Round4();
};