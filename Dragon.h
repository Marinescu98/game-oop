#pragma once
#include "Monster.h"

class Dragon : public Monster
{
	int damageAttackSpecial;
public:
	Dragon(int armour, int damage, int xp, int atk) : Monster(armour,damage,xp),damageAttackSpecial(atk)
	{
	}
	void PrintStats() override;
	int GetDamageAttackSpecial()
	{
		return damageAttackSpecial;
	}
	void dragonFly()
	{
		hp += 20;
		armour += 10;
	}
};