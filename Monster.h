#pragma once
#include "Entity.h"

class Player;

class Monster : public Entity
{
	int xpForPlayer;
public:

	Monster(int=0, int=0, int=0);

	inline void setXpForPlayer(const int& value)
	{
		xpForPlayer = value;
	}

	int GetXpForPlayer()
	{
		return xpForPlayer;
	}
	virtual void PrintStats();
};