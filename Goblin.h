#pragma once
#include "Monster.h"
class Goblin :
	public Monster
{
public:
	Goblin(int=0,int=0,int=0); 

	void PrintStats() override {
		cout << " Goblin \n";
		Monster::PrintStats();
	}

	~Goblin();
};

