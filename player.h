#pragma once
#include<string>
#include "Entity.h"
#include "Monster.h"

class Player : public Entity
{
private:
	string name;
	int mana;
	int energy;
	int xp;
	int level;
	int energyForAttack;
	int manaForAttckSpecial;
	bool attckSpecial;
	int damageAttackSpecial;
	bool run;
public:
	Player();

	void Run();

	int getHp()
	{
		return hp;
	}

	inline void addArmor(const int& x)
	{
		armour += x;
	}
	inline void addMana(const int& x)
	{
		mana += x;
	}

	inline void addHp(const int& x)
	{
		hp += x;
	}

	inline void setDamage(const int& x)
	{
		damage = x;
	}

	void setName(const string& n)
	{
		name = n;
	}

	string getName()
	{
		return name;

	}

	void Magic(Monster&);

	void Attack(Monster&);

	void SetEnergyXp(int energyA, int xpA)
	{
		energy += energyA;
		xp += xpA;
		if (xp > 100)
		{
			xp = 0;
			level++;
		}
	}

	void PrintStats();

	void setAttackSpecial()
	{
		attckSpecial = true;
	}

	void setRun()
	{
		run = true;
	}

	bool CanAttack()
	{
		return attckSpecial;
	}

	bool CanRun()
	{
		return run;
	}
};
