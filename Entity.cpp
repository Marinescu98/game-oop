#include "Entity.h"

Entity::Entity(int armour, int damage)
	: hp(100)
	, armour(armour)
	, damage(damage)
	, alive(true)
{
}

bool Entity::TakeDamage(const int& damage)
{
	if (armour >= damage)
	{
		armour -= damage;
	}
	else
	{
		hp -= damage - armour;
		armour = 0;
	}

	if (hp <= 0)
	{
		alive = false;
	}

	return alive;
}